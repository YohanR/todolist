
var class_fait = "fait";
var class_non_fait = "nonfait";
var table_id = "tableau";
var input_id = "monentree";



function rayer(element) 
{
  if (hasClass(element, class_fait))  {
    element.classList.replace(class_fait, class_non_fait);
  }else if (hasClass(element, class_non_fait)){
    element.classList.replace(class_non_fait, class_fait);
  }
}

function hasClass(elt, className) 
{
    return elt.classList.contains(className);
}

function ajout() 
{
  const input = document.getElementById(input_id);
  const table = document.getElementById(table_id);
  const li = document.createElement("li");
  const button = document.createElement("button");
  
  li.classList.add(class_non_fait);
  li.appendChild(document.createTextNode(input.value + "\n"));
  li.onclick = function(){rayer(this)};

  button.appendChild(document.createTextNode('x'));
  button.onclick = function(){remove(this)};
  
  table.appendChild(li);
  li.appendChild(button);

}

function remove(element)
{
  const li = element.parentNode;
  li.remove();
}
 
